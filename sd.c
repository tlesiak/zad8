#include "headers.h"

float Sd(float x[], int elem){
    int i = 0;
    float res = 0.0;
    float variance = 0.0;
    float mean = Mean(x,elem);

    for(i=0;i<elem;i++){
        variance += pow(x[i] - mean,2);
    }
    
    return sqrt(variance/elem);
}