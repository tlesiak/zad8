#include <stdio.h>
#include <stdlib.h>
#include "headers.h"

struct dataFromFile
{
    float data[50];
    float mean;
    float median;
    float sd;
};

int GetNumberOfCharsInFile(FILE* file){
    if (file == NULL){
      return 0;
    }
  
    fseek(file, 0, SEEK_END);
    int numberOfChars = ftell(file);
    printf("Number of Chars = %d\n", numberOfChars);

    return numberOfChars;
}

int main()
{
    int numberOfElements = 50;
    char a[256] = "\0";
    int i = 0;
  	int numberOfExpectedChars = 0;
    int numberOfChars = 0;
    
    struct dataFromFile dataX;
    struct dataFromFile dataY;
    struct dataFromFile dataRHO;

    FILE *file;

    // Sprawdzenie ilości znaków w pliku wzorcowym

    file = fopen("P0001_attr_default.rec","r");
    numberOfExpectedChars = GetNumberOfCharsInFile(file);
    fclose(file);

    // Sprawdzenie ilości znaków w pliku, w którym mogły wystąpić zmiany

    file = fopen("P0001_attr.rec","r");
    numberOfChars = GetNumberOfCharsInFile(file);
    rewind(file);

    //-----------------------------------------------------------------

    if(numberOfChars == numberOfExpectedChars){
      fgets(a,256,file); // Pominięcie pierwszego rzędu
      while(fscanf(file, "%s %f %f %f", a, &dataX.data[i], &dataY.data[i], &dataRHO.data[i]) !=EOF) // Przypisanie każdego kolejnego rzędu do odpowiednich tablic aż do końca pliku (EOF)
      { 
          i++;
      }

      fclose(file);

      // X - statystki opisowe

      dataX.mean = Mean(dataX.data,numberOfElements);
      dataX.sd = Sd(dataX.data,numberOfElements);
      dataX.median = Median(dataX.data,numberOfElements);

      printf("Mean of X = %.3f\n", dataX.mean);
      printf("Standard deviation of X = %.3f\n", dataX.sd);
      printf("Median of X = %.3f\n", dataX.median);

      // Y - statystki opisowe

      dataY.mean = Mean(dataY.data,numberOfElements);
      dataY.sd = Sd(dataY.data,numberOfElements);
      dataY.median = Median(dataY.data,numberOfElements);
      
      printf("Mean of X = %.3f\n", dataY.mean);
      printf("Standard deviation of X = %.3f\n", dataY.sd);
      printf("Median of X = %.3f\n", dataY.median);

      // RHO - statystki opisowe

      dataRHO.mean = Mean(dataRHO.data,numberOfElements);
      dataRHO.sd = Sd(dataRHO.data,numberOfElements);
      dataRHO.median = Median(dataRHO.data,numberOfElements);
      
      printf("Mean of X = %.3f\n", dataRHO.mean);
      printf("Standard deviation of X = %.3f\n", dataRHO.sd);
      printf("Median of X = %.3f\n", dataRHO.median);

      //------------------------

      file = fopen("P0001_attr.rec","a"); // otwarcie w celu wpisania na końcu pliku

      fprintf(file, "\nMean  \t%.5f\t%.5f\t%.3f\n",dataX.mean,dataY.mean,dataRHO.mean);
      fprintf(file, "Sd    \t%.5f\t%.5f\t%.3f\n",dataX.sd,dataY.sd,dataRHO.sd);
      fprintf(file, "Median\t%.5f\t%.5f\t%.3f",dataX.median,dataY.median,dataRHO.median);
      
      fclose(file);
    }else{
      fclose(file);
      printf("Statystyki zostaly juz dopisane\n");
    }
    
    system("PAUSE");
    return 0;
}