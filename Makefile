CC=gcc
CFLAGS=-Wall 
LIBS=-lm 

all: main main2

main: main.o mean.o sd.o median.o 
	$(CC)	$(CFLAGS) -o main main.o mean.o sd.o median.o $(LIBS)

main2: main2.o mean.o sd.o median.o 
	$(CC)	$(CFLAGS) -o main2 main2.o mean.o sd.o median.o $(LIBS)

main.o: main.c headers.h 
	$(CC)	$(CFLAGS) -c main.c

main2.o: main2.c headers.h 
	$(CC)	$(CFLAGS) -c main2.c

mean.o: mean.c 
	$(CC)	$(CFLAGS) -c mean.c

sd.o: sd.c 
	$(CC)	$(CFLAGS) -c sd.c

median.o: median.c 
	$(CC)	$(CFLAGS) -c median.c