#include <stdio.h>
#include <stdlib.h>
#include "headers.h"

int main()
{
    int numberOfElements = 50;
    char a[256] = "\0";
    int i = 0;
    float *x;
    float *y;
    float *RHO;
    x = (float *)calloc(numberOfElements,sizeof(float));
    y = (float *)calloc(numberOfElements,sizeof(float));
    RHO = (float *)calloc(numberOfElements,sizeof(float));

    FILE * file;
    file = fopen("P0001_attr.rec","r");
    fgets(a,256,file); // Pominięcie pierwszego rzędu
    while(fscanf(file, "%s %f %f %f", a, &x[i], &y[i], &RHO[i]) !=EOF)// Przypisanie każdego kolejnego rzędu do odpowiednich tablic aż do końca pliku (EOF)
    { 
        i++;
    }
       
    fclose(file);

    // X - statystki opisowe

    printf("Mean of X = %.3f\n", Mean(x,numberOfElements));
    printf("Standard deviation of X = %.3f\n", Sd(x,numberOfElements));
    printf("Median of X = %.3f\n", Median(x,numberOfElements));

    // Y - statystki opisowe

    printf("Mean of Y = %.3f\n", Mean(y,numberOfElements));
    printf("Standard deviation of Y = %.3f\n", Sd(y,numberOfElements));
    printf("Median of Y = %.3f\n", Median(y,numberOfElements));

    // RHO - statystki opisowe

    printf("Mean of RHO = %.3f\n", Mean(RHO,numberOfElements));
    printf("Standard deviation of RHO = %.3f\n", Sd(RHO,numberOfElements));
    printf("Median of RHO = %.3f\n", Median(RHO,numberOfElements));

    //------------------------

    file = fopen("P0001_attr.rec","a"); // otwarcie w celu wpisania na końcu pliku

    fprintf(file, "\nMean  \t%.5f\t%.5f\t%.3f\n",Mean(x,numberOfElements),Mean(y,numberOfElements),Mean(RHO,numberOfElements));
    fprintf(file, "Sd    \t%.5f\t%.5f\t%.3f\n",Sd(x,numberOfElements),Sd(y,numberOfElements),Sd(RHO,numberOfElements));
    fprintf(file, "Median\t%.5f\t%.5f\t%.3f",Median(x,numberOfElements),Median(y,numberOfElements),Median(RHO,numberOfElements));
    
    fclose(file);

    free(x);
    free(y);
    free(RHO);
    system("PAUSE");
    return 0;
}